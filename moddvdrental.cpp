#include <bits/stdc++.h> 
#include<iostream>
#include<string>
using namespace std;

 
class Node{
public:
    string data;
    Node * next;
};



 
int main () {
    string movies[26] = {"0","1.) Deadpool 1", "2.) Shazam!", "3.) Alita: Battle Angel", "4.) Happy Death Day 2U", "5.) Annabelle Comes Home", "6.) Dumbo", "7.) Pokemon Detective Pikachu", "8.) John Wick: Chapter 3", "9.)Aladdin", "10.) Godzilla: King of the Monsters", "11.) Spiderman: Far From Home", "12.) The Lion King", "13.) Fantastic Beasts: The Crimes of Grindelwald", "14.) Jurassic World: Fallen Kingdom", "15.) Fifty Shades Freed", "16.) Ant-Man and The Wasp", "17.) Hotel Transylvania 3: Summer Vacation", "18.) Ralph Breaks The Internet", "19.) Maze Runner: The Death Cure", "20.) Ready Player One", "21.) The Nutcracker and the Four Realms", "22.) Incredibles 2", "23.) Mission: Impossible-Fallout", "24.) Venom", "25.) Insidious: The Last Key"};
    string status[26] = {"0","Available    ","Available    ","Available    ","Available    ","Available    ","Available    ","Available    ","Available    ","Available    ","Available    ","Available    ","Available    ","Available    ","Available    ","Available    ","Available    ","Available    ","Available    ","Available    ","Available    ","Available    ","Available    ","Available    ","Available    ","Available    "};
    string directors[26] = {"0","Tim Miller", "David Sandberg", "Robert Rodriguez", "Christopher Landon", "Gary Dauberman", "Tim Burton", "Rob Letterman", "Chad Stahelski", "Guy Ritchie", "Michael Dougherty", "Jon Watts", "Jon Favreau", "David Yates", "J.A. Bayona", "James Foley", "Peyton Reed", "Genndy Tartakovsky", "Rich Moore", "Wes Ball", "Steven Spielberg", "Lasse Hallström", "Brad Bird", "Christopher McQuarrie", "Ruben Fleischer", "Adam Robitel"};
    string writers[26] = {"0","Ryan Reynolds", "Peter Sefran", "James Cameron", "Jason Blum", "James Wan", "Justin Springer", "Mary Parent", "Basil Iwanyk", "Dan Lin", "Mary Parent", "Kevin Feige", "Jon Favreau", "J.K. Rowling", "Frank Marshall", "Michael De Luca", "Kevin Feige", "Michelle Murdocca", "Clark Spencer", "Ellen Goldsmith-Vein", "Donald De Line", "Mark Gordon", "John Walker", "J.J. Abrams", "Avi Arad", "Jason Blum",};
    string stars[26] = {"0","Ryan Reynolds, Morena Baccarin, Ed Skrein, TJ Miller, Gina Carano, Brianna Hildebrand", "Zachary Levi, Mark Strong, Asher Angel, Jack Grazer, Adam Brody, Djimon Hounsou", "Rosa Salazar, Christoph Waltz, Jennifer Connelly, Mahershala Ali, Ed Skrein, Jackie Earle Haley, Keean Johnson", "Jessica Rothe, Israel Broussard, Suraj Sharma, Steve Zissis", "McKenna Grace, Madison Iseman, Katie Sarife, Patrick Wilson, Vera Farmiga", "Collin Farrell, Michael Keaton, Danny DeVito, Eva Green, Alan Arkin", "Ryan Reynolds, Justice Smith, Kathryn Newton, Suki Waterhouse, Omar Chaparro, Chris Geere, Ken Watanabe, Bill Nighy", "Keanu Reeves, Halle Berry, Laurence Fishburne, Mark Dacascos, Asia Kate Dillon, Lance Reddick, Anjelica Huston, Ian McShane", "Will Smith, Mena Massoud, Naomi Scott, Marwan Kenzari, Navid Negahban, Nasim Pedrad, Billy Magnussen", "Kyle Chandler, Vera Farmiga, Millie Bobby Brown, Bradley Whitford, Sally Hawkins, Charles Dance, Thomas Middleditch, Aisha Hinds, O'Shea Jackson Jr., David Strathaim, Ken Watanabe, Zhang Ziyi", "Tom Holland, Sammuel Jackson, Zendaya, Cobie Smulders, Jon Favreau, J.B. Smoove, Jacob Batalon, Martin Starr, Marisa Tomei, Jake Gyllenhaal", "Donald Glover, Seth Rogen, Chiwetel Ejiofor, Alfre Woodard, Billy Eichner, John Kani, John Oliver, Beyonce Knowles-Carter, James Earl Jones", "Eddie Redmayne, Katherine Waterston, Dan Fogler, Alison Sudol, Ezra Miller, Zoe Kravitz, Callum Turner, Claudia Kim, William Nadylam, Kevin Guthrie, Jude Law, Johnny Depp", "Chris Pratt, Bryce Dallas Howard, Rafe Spall, Justice Smith, Daniella Pineda, James Cromwell, Toby Jones, Ted Levine, BD Wong, Isabella Sermon, Geraldine Chaplin, Jeff Goldblum", "Dakota Johnson, Jamie Dornan, Eric Johnson, Rita Ora, Luke Grimes, Victor Rasuk, Jennifer Ehle, Marcia Gay Harden", "Paul Rudd, Evangeline Lilly, Michael Peña, Walton Goggins, Bobby Cannavale, Judy Geer, Tip Harris, David Dastmalchian, Hannah John-Kamen, Abby Ryder Fortson, Randall Park, Michelle Pfeiffer, Laurence Fishburne, Michael Douglas", "Adam Sandler, Andy Samberg, Selena Gomez, Kevin James, David Spade, Steve Buscemi, Keegan-Michael Key, Molly Shannon, Fran Drescher, Kathryn Hahn, Jim Gaffigan, Mel Brooks", "John Reilly, Sarah Silverman, Gal Gadot, Taraji Henson, Jack McBrayer, Jane Lynch, Alan Tudyk, Alfred Molina, Ed O'Neill", "Dylan O'Brien, Kaya Scodelario, Thomas Brodie-Sangster, Nathalie Emmanuel, Giancarlo Esposito, Aidan Gillen, Walton Goggins, Ki Hong Lee, Barry Pepper, Will Poulter, Patricia Clarkson", "Tye Sheridan, Olivia Cooke, Ben Mendelsohn, T.J. Miller, Simon Pegg, Mark Rylance", "Keira Knightley, Mackenzie Foy, Eugenio Derbez, Matthew Macfadyen, Richard Grant, Misty Copeland, Helen Mirren, Morgan Freeman", "Craig Nelson, Holly Hunter, Sarah Vowell, Huckleberry Milner, Samuel Jackson", "Tom Cruise, Henry Cavill, Ving Rhames, Simon Pegg, Rebecca Ferguson, Sean Harris, Angela Bassett, Michelle Monaghan, Alec Baldwin", "Tom Hardy, Michelle Williams, Riz Ahmed, Scott Haze, Reid Scott", "Lin Shaye, Angus Sampson, Leigh Whannell, Spencer Locke, Caitlin Gerard, Bruce Davison",};
    
    int b;
    Node* name = NULL;
    Node* movie = NULL;
    name = new Node();
    movie = new Node();
    int choice;
    char enter = 'y';
    char stay;
    while(enter == 'y'||enter == 'Y'){
        cout<<"DVD RENTAL\n";
        cout<<"Continue? (y/n) > ";
        cin>>stay;
        while(stay == 'y' || stay == 'Y'){
            system("clear");
            cout<<"1.) Borrow a Movie\n2.) Return a Movie\n3.) Search for Movie\n4.) Check Out\n";
            cout<<"Enter Selection Number > ";
            cin>>choice;
            switch(choice){
                case 1:
                    for(int a = 1;a < 26 ; ++a){
                        cout<<status[a]<<movies[a]<<endl;
                    }
                    cout << "Enter Name > ";
                    cin.ignore();
                    getline(cin,name->data);
                    cout << "Enter Movie Number > ";
                    cin >> b;
                    if((b > 26)||(b == 0)){
                        cout<<"Movie Doesn't Exist\n";
                    }
                    else{
                        status[b] = "Unavailable  ";
                        for(int c = 1;c < 26 ; ++c){
                            cout<<status[c]<<movies[c]<<endl;
                        }
                        cout<<"Press any key to continue...";
                        cin.ignore();
                        cin.get();
                        name->next = movie;
                        movie->data = movies[b];
                    }
                break;
                case 2://return
                    int r;
                    cout<<"Enter Number of Movie you want to return > ";
                    cin >> r;
                    if((status[r] == "Available    ")){
                        cout<<"Movie is already available.\nRetun Unsuccessful.\n";
                    }
                    else if((r >= 7)||(r == 0)){
                        cout<<"Movie Doesn't Exist.\n";
                    }
                    else{
                    status[r] = "Available    ";
                    for(int d = 1;d < 26 ; ++d){
                        cout<<status[d]<<movies[d]<<endl;
                    }
                    cin.ignore();
                    cin.get();
                    system("clear");
                    movie->data = movies[r];
                    }
                break;
                case 3://Search
                {
                    string mov;
		            cout<<"Enter a Movie > "; 
    		        cin.ignore();
    		        getline(cin,mov);
    		        cout<<"Results for \""<<mov<<"\"\n";
    	            for (int e = 1; e < 26; e++){
    		            if(movies[e].find(mov, 0) != std::string::npos){
    		                cout<<status[e]<< movies[e]<< endl;
			            }
			        }
                }
                break;
                case 4://CheckOut
                    system("clear");
                    movie->data = movies[b];
                    if(movie->data == "0"){
                        cout<<"Nothing to see here\n";
                        }
                    else{
                        cout<<name->data<<endl;
                        cout<<movie->data<<endl;
                    }
            }
        cout<<"Stay? (y/n) > ";
        cin>>stay;
        cin.ignore();
        system("clear");
        }
    cout<<"Enter Video City? (y/n) > ";
    cin>>enter;
    }
return 0;

}